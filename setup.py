# coding: utf-8

import sys
from setuptools import setup, find_packages

NAME = "fill_wikipedia"
VERSION = "0.2.0"



REQUIRES = [
    "click",
    "osmapi",
    "requests",
    "overpy",
    "tqdm",
    "colorama"
]

setup(
    name=NAME,
    version=VERSION,
    description="CLI tool to fill the wikipedia tag",
    author_email="xbarnada@protonmail.com",
    url="",
    keywords=["OpenStreetMap", "Wikipedia"],
    install_requires=REQUIRES,
    packages=find_packages(),
    package_data={},
    include_package_data=True,
    entry_points={
        'console_scripts': ['fillwikipedia=fill_wikipedia.cli:fill_wikipediacommand']},
    long_description="CLI tool to fill the wikipedia tag"
)

